--- Required libraries.

-- Localised functions.
local atan2 = math.atan2
local sqrt = math.sqrt
local abs = math.abs

-- Localised values.
local pi = math.pi

--- Class creation.
local library = {}

-- Static values.
library.ButtonState = {}
library.ButtonState.Released = "released"
library.ButtonState.Pressed = "pressed"
library.ButtonState.JustReleased = "justReleased"
library.ButtonState.JustPressed = "justPressed"

library.ButtonPhase = {}
library.ButtonPhase.Down = "down"
library.ButtonPhase.Up = "up"

library.MouseButton = {}
library.MouseButton.Primary = "primary"
library.MouseButton.Secondary = "secondary"
library.MouseButton.Middle = "middle"

library.MouseScroll = {}
library.MouseScroll.Left = "scrollLeft"
library.MouseScroll.Right = "scrollRight"
library.MouseScroll.Up = "scrollUp"
library.MouseScroll.Down = "scrollDown"

library.MouseScrollDirection = {}
library.MouseScrollDirection.Left = -1
library.MouseScrollDirection.Right = 1
library.MouseScrollDirection.Up = 1
library.MouseScrollDirection.Down = -1

library.ThumbStick = {}
library.ThumbStick.Left = "left"
library.ThumbStick.Right = "right"

library.ControllerType = {}
library.ControllerType.Nimbus = "Nimbus"
library.ControllerType.SwitchPro = "Pro Controller"


--- Initialises this Scrappy library.
function library:init()

	-- Register the mouse listener
	Runtime:addEventListener( "mouse", self )

	-- Register the key listener
	Runtime:addEventListener( "key", self )

	-- Register the axis listener
	Runtime:addEventListener( "axis", self )

	-- Tables to store button phases and states
	self._buttonPhases = {}
	self._buttonStates = {}

	-- Table to store named actions
	self._actions = {}

	-- Table to control mappings
	self._mappings = {}

	for i = 1, 4, 1 do

		self._buttonPhases[ i ] = {}
		self._buttonStates[ i ] = {}

		self._actions[ i ] = {}

		self._mappings[ i ] = {}

		-- Initialise the states and phases for each mouse button
		for k, v in pairs( library.MouseButton ) do
	    	self._buttonStates[ i ][ v ] = library.ButtonState.Released
	        self._buttonPhases[ i ][ v ] = library.ButtonPhase.Up
	    end

	end

	-- Initialise the stick tables
	self._sticks = {}
	for i = 1, 4, 1 do
		self._sticks[ i ] = {}
		for k, v in pairs( library.ThumbStick ) do
			self._sticks[ i ][ v ] = self._sticks[ i ][ v ] or { x = 0, y = 0 }
		end
	end

end

--- Registers a named action.
-- @param name The name of the action.
-- @param button The name of the button to pair with this action. Can also be a table containing multiple buttons.
-- @param playerNumber The number of the player. Optional, defaults to 1.
function library:registerAction( name, button, playerNumber )

	-- Was a button specified?
	if button then

		-- Is the button a string type?
		if type( button ) == "string" then
			-- Call the function again, passing it in a lonely table
			self:registerAction( name, { button }, playerNumber )
		else
			-- Register the buttons
			self._actions[ playerNumber or 1 ][ name ] = button
		end

	-- No button specified
	else
		-- Guess we're clearing the action
		self._actions[ playerNumber or 1 ][ name ] = button
	end

end

-- Gets any buttons associated with this action.
-- @param name The name of the action.
-- @param playerNumber The number of the player. Optional, defaults to 1.
-- @return The registered buttons, or a table containing just the passed in name if none found.
function library:getAction( name, playerNumber )
	return self._actions[ playerNumber or 1 ][ name ] or { name }
end

-- Get the named actions that's been paired with a specific button.
-- @param name The name of the button.
-- @param playerNumber The number of the player. Optional, defaults to 1.
-- @return The registered actions, or a table just containing the the passed in name if none found.
function library:getActionsFromButton( name, playerNumber )

	local actions = {}

	-- Convert passed in raw buttons/keys
	for k, v in pairs( self._actions[ playerNumber or 1 ] ) do

		-- Loop through all buttons with this action
		for i = 1, #v, 1 do
			if v[ i ] == name then
				actions[ #actions + 1 ] = k
			end
		end

	end

	-- Loop through all actions
	for k, v in pairs( self._actions[ playerNumber or 1 ][ name ] or {} ) do

		-- Loop through all buttons with this action
		for i = 1, #v, 1 do

			-- Do we find a match?
			if v[ i ] == name then

				-- Return the action
				actions[ #actions + 1 ] = v[ i ]

 			end

		end

	end

	-- If not then return the passed in name
	return #actions > 0 and actions or { name }

end

--- Gets the normalised values of a thumbstick.
-- @param stick The name of the stick - Scrappy.Input.ThumbStick.Left or Scrappy.Input.ThumbStick.Right.
-- @param playerNumber The number of the player. Optional, defaults to 1.
-- @return Table containing the normalised X and Y values.
function library:getStickValues( stick, playerNumber )
	return self._sticks[ playerNumber or 1 ][ stick ]
end

--- Gets the angle of a thumbstick.
-- @param stick The name of the stick - Scrappy.Input.ThumbStick.Left or Scrappy.Input.ThumbStick.Right.
-- @param offset The rotation offset of the controller. Optional, defaults to 0.
-- @param playerNumber The number of the player. Optional, defaults to 1.
-- @return The angle.
function library:getStickAngle( stick, offset, playerNumber )

	local values = self:getStickValues( stick, playerNumber )

	if values then

		if values.x ~= 0 or values.y ~= 0 then

			local angleInRadians = atan2( values.y, values.x )
 			local angleInDegrees = ( 180 / pi ) * angleInRadians

			local compassRadians = pi / 2 - angleInRadians
 			local compassDegrees = ( 180 / pi ) * compassRadians

			compassDegrees = compassDegrees - 180

			local compassAngle = ( ( compassDegrees % 360 ) + 360 ) % 360

			compassAngle = abs( compassAngle - 360 )

			return compassAngle + ( offset or 0 )

		end

	end

end

--- Gets the distance the stick is away from the centre.
-- @param stick The name of the stick - Scrappy.Input.ThumbStick.Left or Scrappy.Input.ThumbStick.Right.
-- @param playerNumber The number of the player. Optional, defaults to 1.
-- @return The distance.
function library:getStickDistance( stick, playerNumber )

	local values = self:getStickValues( stick, playerNumber )

	if values then
		return sqrt( values.x * values.x + values.y * values.y )
	end

end

--- Sets a button phase.
-- @param button The name of the button.
-- @param phase The name of the phase, for options see Scrappy.Input.ButtonPhase.
-- @param playerNumber The number of the player. Optional, defaults to 1.
function library:setButtonPhase( name, phase, playerNumber )
	if not self:isButtonInState( name, phase, playerNumber ) then
		self._buttonPhases[ playerNumber or 1 ][ name ] = phase
	end
end

--- Gets a button phase.
-- @param name The name of the button.
-- @param playerNumber The number of the player. Optional, defaults to 1.
-- @return The current phase.
function library:getButtonPhase( name, playerNumber )
	return self._buttonPhases[ playerNumber or 1 ][ name ]
end

--- Checks if a button is currently in a specific phase.
-- @param name The name of the button.
-- @param phase The name of the phase, for options see Scrappy.Input.ButtonPhase.
-- @param playerNumber The number of the player. Optional, defaults to 1.
-- @return True if it is, false otherwise.
function library:isButtonInPhase( name, phase, playerNumber )
	return self:getButtonPhase( name, playerNumber ) == phase
end

--- Sets a button state.
-- @param button The name of the button.
-- @param state The name of the state, for options see Scrappy.Input.ButtonState.
-- @param playerNumber The number of the player. Optional, defaults to 1.
function library:setButtonState( name, state, playerNumber )

	-- Convert the name if it's mapped
	name = self:getMappedName( name )

	if name and not self:isButtonInState( name, state, playerNumber ) then

		local actions = self:getActionsFromButton( name, playerNumber )

		for i = 1, #actions, 1 do

			-- Fire off a 'button' event with the button name, or paired action, and new state
			Runtime:dispatchEvent{ name = "button", button = actions[ i ], raw = name, state = state, playerNumber = playerNumber or 1 }

		end

		-- Store out the state
		self._buttonStates[ playerNumber or 1 ][ name ] = state

	end

end

--- Gets a button state.
-- @param name The name of the button.
-- @param playerNumber The number of the player. Optional, defaults to 1.
-- @return The current state.
function library:getButtonState( name, playerNumber )

	-- Convert the name if it's mapped
	name = self:getMappedName( name )

	return self._buttonStates[ playerNumber or 1 ][ name ]

end

--- Checks if a button is currently in a specific state.
-- @param name The name of the button. Optional, will check all buttons if nil.
-- @param state The name of the state, for options see Scrappy.Input.ButtonState.
-- @param playerNumber The number of the player. Optional, defaults to 1.
-- @return True if it is, false otherwise. If no button was passed in and one is pressed it'll also return the button name.
function library:isButtonInState( name, state, playerNumber )

	-- Was a name passed in?
	if name then
		
		local actions = self:getActionsFromButton( name, playerNumber )
		
		-- Loop through the named actions for this button
		for k, v in pairs( self:getAction( name, playerNumber ) ) do
		
			-- Check if this button is in the state, if so then return true as we only need one
			if self:getButtonState( v, playerNumber ) == state then
				return true
			end
		
		end
		
		-- No buttons are in the state
		return false
		
	else
		return self:isAnyButtonInState( state, playerNumber )
	end
	
end

--- Checks if any button is currently in a specific state.
-- @param state The name of the state, for options see Scrappy.Input.ButtonState.
-- @param playerNumber The number of the player. Optional, defaults to 1.
-- @return True if it is, false otherwise.
function library:isAnyButtonInState( state, playerNumber )

	local button
	
	for k, v in pairs( self._buttonStates[ playerNumber or 1 ] ) do
		
		if self:isButtonInState( k, state, playerNumber ) then
			button = k
			break
		end
	end
	
	return button ~= nil, button

end

--- Checks if a button is currently pressed.
-- @param name The name of the button. Optional, will check all buttons if nil.
-- @return True if it is, false otherwise.
function library:isButtonPressed( name, playerNumber )
	return self:isButtonInState( name, library.ButtonState.Pressed, playerNumber )
end

--- Checks if a button is currently released.
-- @param name The name of the button. Optional, will check all buttons if nil.
-- @return True if it is, false otherwise.
function library:isButtonReleased( name, playerNumber )
	return self:isButtonInState( name, library.ButtonState.Released, playerNumber )
end

--- Checks if a button was just pressed.
-- @param name The name of the button. Optional, will check all buttons if nil.
-- @return True if it was, false otherwise.
function library:wasButtonJustPressed( name, playerNumber )
	return self:isButtonInState( name, library.ButtonState.JustPressed, playerNumber )
end

--- Checks if a button was just released.
-- @param name The name of the button. Optional, will check all buttons if nil.
-- @return True if it was, false otherwise.
function library:wasButtonJustReleased( name, playerNumber )
	return self:isButtonInState( name, library.ButtonState.JustReleased, playerNumber )
end

--- Gets the current position of the mouse.
-- @param asTable True if you want to get the position as a table, false if you want seperate values. Optional, defaults to false.
-- @return The position, or the centre of the screen if not set.
function library:getMousePosition( asTable )
	if asTable then
		return self._mousePosition
	else
		if self._mousePosition then
			return self._mousePosition.x, self._mousePosition.y
		end
	end
end

--- Gets the current X position of the mouse.
-- @return The position, or the centre of the screen if not set.
function library:getMouseX()
	return self._mousePosition and self._mousePosition.x or display.contentCenterX
end

--- Gets the current Y position of the mouse.
-- @return The position, or the centre of the screen if not set.
function library:getMouseY()
	return self._mousePosition and self._mousePosition.y or display.contentCenterY
end


--- Gets the current X scroll value of the mouse.
-- @return The value, or nil if it isn't set.
function library:getMouseScrollX()
	return self._mouseScroll and self._mouseScroll.x or nil
end

--- Gets the current Y scroll value of the mouse.
-- @return The value, or nil if it isn't set.
function library:getMouseScrollY()
	return self._mouseScroll and self._mouseScroll.y or nil
end

--- Gets the current X scroll direction of the mouse.
-- @return The direction, or nil if it isn't scrolling.
function library:getMouseScrollDirectionX()
	return self._mouseScrollDirection and self._mouseScrollDirection.x or nil
end

--- Gets the current Y scroll direction of the scrolling.
-- @return The direction, or nil if it isn't set.
function library:getMouseScrollDirectionY()
	return self._mouseScrollDirection and self._mouseScrollDirection.y or nil
end

--- Called when the horizontal scroll wheel was just scrolled.
function library:onScrollX()

	if self:getMouseScrollDirectionX() == library.MouseScrollDirection.Left then

		self:setButtonPhase( Scrappy.Input.MouseScroll.Left, library.ButtonPhase.Down )

		local actions = self:getActionsFromButton( library.MouseScroll.Left )

		for i = 1, #actions, 1 do
			self:setButtonState( actions[ i ], library.ButtonPhase.JustPressed )
		end

	elseif self:getMouseScrollDirectionX() == library.MouseScrollDirection.Right then

		self:setButtonPhase( Scrappy.Input.MouseScroll.Right, library.ButtonPhase.Down )

		local actions = self:getActionsFromButton( library.MouseScroll.Right )

		for i = 1, #actions, 1 do
			self:setButtonState( actions[ i ], library.ButtonPhase.JustPressed )
		end

	end

end

--- Called when the vertical scroll wheel was just scrolled.
function library:onScrollY()

	if self:getMouseScrollDirectionY() == library.MouseScrollDirection.Up then

		self:setButtonPhase( Scrappy.Input.MouseScroll.Up, library.ButtonPhase.Down )

		local actions = self:getActionsFromButton( library.MouseScroll.Up )

		for i = 1, #actions, 1 do
			self:setButtonState( action, library.ButtonPhase.JustPressed )
		end

	elseif self:getMouseScrollDirectionY() == library.MouseScrollDirection.Down then

		self:setButtonPhase( Scrappy.Input.MouseScroll.Down, library.ButtonPhase.Down )

		local actions = self:getActionsFromButton( library.MouseScroll.Down )

		for i = 1, #actions, 1 do
			self:setButtonState( action, library.ButtonPhase.JustPressed )
		end

	end

end

--- Checks if the horizontal scroll wheel was just scrolled.
-- @return True if it was, false otherwise.
function library:wasMouseJustScrolledOnX()
	return self._mouseJustScrolledOnX
end

--- Checks if the vertical scroll wheel was just scrolled.
-- @return True if it was, false otherwise.
function library:wasMouseJustScrolledOnY()
	return self._mouseJustScrolledOnY
end

--- Mouse event handler.
-- @param event The event table.
function library:mouse( event )

	-- Store out the mouse position
	self._mousePosition = { x = event.x, y = event.y }
	
	-- Store out the mouse scroll direction
	self._mouseScrollDirection = { x = ( event.scrollX or 0 ) > 0 and library.MouseScrollDirection.Right or library.MouseScrollDirection.Left, y = ( event.scrollY or 0 ) > 0 and library.MouseScrollDirection.Up or library.MouseScrollDirection.Down }

	-- If the mouse isn't currently scrolling in a certain direction then nil it out
	self._mouseScrollDirection.x = event.scrollX == 0 and 0 or self._mouseScrollDirection.x
	self._mouseScrollDirection.y = event.scrollY == 0 and 0 or self._mouseScrollDirection.y

	-- Do we have mouse scroll info?
	if self._mouseScroll then

		-- Is the current scroll x different to the stored one?
		if event.scrollX ~= self._mouseScroll.x then

			-- Then we just scrolled horizontally!
			self:onScrollX()

		end

		-- Is the current scroll y different to the stored one?
		if event.scrollY ~= self._mouseScroll.y then

			-- Then we just scrolled vertically!
			self:onScrollY()

		end

	-- Otherwise this is the first scroll!
	else

		-- Is the current scroll x not 0?
		if event.scrollX ~= 0 then

			-- Then we just scrolled horizontally!
			self:onScrollX()

		end

		-- Is the current scroll y not 0?
		if event.scrollY ~= 0 then

			-- Then we just scrolled vertically!
			self:onScrollY()

		end

	end

	-- Set the current mouse scroll value
	self._mouseScroll = { x = event.scrollX, y = event.scrollY }

	-- Set the mouse button phases
	self:setButtonPhase( library.MouseButton.Primary, event.isPrimaryButtonDown == true and library.ButtonPhase.Down or library.ButtonPhase.Up )
    self:setButtonPhase( library.MouseButton.Secondary, event.isSecondaryButtonDown == true and library.ButtonPhase.Down or library.ButtonPhase.Up )
    self:setButtonPhase( library.MouseButton.Middle, event.isMiddleButtonDown == true and library.ButtonPhase.Down or library.ButtonPhase.Up )

end

--- Key event handler.
-- @param event The event table.
function library:key( event )

	local playerNumber = self:getPlayerNumberFromDevice( event.device ) or 1

	-- Set this key's phase
	self:setButtonPhase( event.keyName, event.phase, playerNumber )

	-- Was this the back button, and should we catch it? Then return true
	return self:shouldBackButtonBeCaught() and event.keyName == "back"

end

--- Axis event handler.
-- @param event The event table.
function library:axis( event )

	local playerNumber = self:getPlayerNumberFromDevice( event.device ) or 1

	self._sticks[ playerNumber ] = self._sticks[ playerNumber ] or {}
	for k, v in pairs( library.ThumbStick ) do
		self._sticks[ playerNumber ][ v ] = self._sticks[ playerNumber ][ v ] or { x = 0, y = 0 }
	end

	if self._mappings[ playerNumber ] and self._mappings[ playerNumber ].axis then
		for stick, indexes in pairs( self._mappings[ playerNumber ].axis ) do
			for axis, index in pairs( indexes ) do
				if event.axis.number == index then
					self._sticks[ playerNumber ][ stick ][ axis ] = event.normalizedValue
				end
			end
		end
	else
		if event.axis.number == 1 or event.axis.number == 3 then
			self._sticks[ playerNumber ][ library.ThumbStick.Left ].x = event.normalizedValue
			self._sticks[ playerNumber ][ library.ThumbStick.Right ].x = event.normalizedValue
		elseif event.axis.number == 2 or event.axis.number == 4 then
			self._sticks[ playerNumber ][ library.ThumbStick.Left ].y = event.normalizedValue
			self._sticks[ playerNumber ][ library.ThumbStick.Right ].y = event.normalizedValue
		end
	end

	Runtime:dispatchEvent{ name = "thumbstick", values = self._sticks[ playerNumber ], player = playerNumber }

end

--- Update event handler. You must call this!
function library:update( dt )

	for i = 1, #self._buttonPhases, 1 do

		-- Loop through all button phases
		for button, phase in pairs( self._buttonPhases[ i ] ) do

			-- Get the state of this button
			local state = self:getButtonState( button, i )

			-- Do we have a state?
			if state then

				-- Is the button currently down?
				if phase == library.ButtonPhase.Down then

					-- Was it just released or just pressed?
					if state == library.ButtonState.JustPressed or state == library.ButtonState.JustReleased then

						-- Mark it as being pressed
						self:setButtonState( button, library.ButtonState.Pressed, i )

					-- Otherwise was it already released?
					elseif state == library.ButtonState.Released then

						-- Mark it as being just pressed
						self:setButtonState( button, library.ButtonState.JustPressed, i )

					end

				-- Is the button currently up?
				elseif phase == library.ButtonPhase.Up then

					-- Was it just released?
					if state == library.ButtonState.JustReleased then

						-- Then mark it as released
						self:setButtonState( button, library.ButtonState.Released, i )

					-- Otherwise was it pressed or just pressed?
					elseif state == library.ButtonState.Pressed or state == library.ButtonState.JustPressed then

						-- Then mark it as just released
						self:setButtonState( button, library.ButtonState.JustReleased, i )

					end

				end

			-- We don't have a current state
			else

				-- Is the button currently down?
				if phase == library.ButtonPhase.Down then

					-- Set the button as just pressed
					self:setButtonState( button, library.ButtonState.JustPressed, i )

				-- Otherwise is the button released?
				elseif phase == library.ButtonPhase.Up then

					-- Mark it as being just released
					self:setButtonState( button, library.ButtonState.JustReleased, i )

				end

			end

		end

	end


	for k, v in pairs( library.MouseScroll ) do
		if self:getButtonState( v ) == "pressed" then
			self:setButtonPhase( v, library.ButtonPhase.Up )
		end
	end

end

--- Checks, or sets, whether the Android back button should be caught.
-- @param value True if it should be, or false if not. Or leave out if you just want to check the current value.
-- @return True if it should, false otherwise. Or, if a value was passed in then nothing will be returned.
function library:shouldBackButtonBeCaught( value )

	-- Was no value passed in?
	if value == nil then

		-- Then just return the current value
		return self._catchBackButton

	else -- Otherwise

		-- Set the value
		self._catchBackButton = value

	end

end

--- Gets the mapped name of this button.
-- @param name The name of the button.
-- @param playerNumber The number of the player. Optional, defaults to 1.
-- @return The mapped name, or the passed in name if it isn't mapped.
function library:getMappedName( name, playerNumber )
	return self._mappings and self._mappings[ playerNumber or 1 ][ name ] or name
end

--- Sets the current controller mappings.
-- @param playerNumber The number of the player. Optional, defaults to 1.
-- @param mappings The mappings.
function library:mapControls( mappings, playerNumber )
	self._mappings[ playerNumber or 1 ] = mappings
end

--- Gets a player number from a device.
-- @param device The device object.
-- @return The player number, or 1 if it can't be found.
function library:getPlayerNumberFromDevice( device )

	if device then

		if device.playerNumber then
			return device.playerNumber
		else
			return tonumber( device.descriptor:gsub( "%D+" , "" ) or "" )
		end

	end

	return 1

end

--- Gets the input device in use for a player number.
-- @param playerNumber The player number. Optional, defaults to 1.
-- @return The found input device.
function library:getDeviceFromPlayerNumber( playerNumber )

	local devices = system.getInputDevices()

	for i = 1, #( devices or {} ), 1 do
		if self:getPlayerNumberFromDevice( devices[ i ] ) == ( playerNumber or 1 ) then
			return devices[ i ]
		end
	end

end

--- Gets the input device type in use for a player number.
-- @param playerNumber The player number. Optional, defaults to 1.
-- @return The controller type from Scrappy.Input.ControllerType, or the displayName if it doesn't match any known ones yet.
function library:getControllerTypeFromPlayerNumber( playerNumber )

	local device = self:getDeviceFromPlayerNumber( playerNumber )

	if device then

		for k, v in pairs( library.ControllerType ) do
			if device.displayName == v then
				return v
			end
		end

		return device.displayName

	end

end

--- Vibrates the controller a player is using, if it can be vibrated.
-- @param playerNumber The player number. Optional, defaults to 1.
function library:vibrateDevice( playerNumber )

	local device = self:getDeviceFromPlayerNumber( playerNumber )

	if device and device.canVibrate then
		device:vibrate()
	end

end

--- Gets the number of players currently connected.
-- @return The player count.
function library:getPlayerCount()

	local count = 0

	local counted = {}

	local devices = system.getInputDevices()

	for i = 1, #( devices or {} ), 1 do

		local playerNumber = self:getPlayerNumberFromDevice( devices[ i ] )

		if playerNumber and not counted[ playerNumber ] then
			counted[ tostring( playerNumber ) ] = true
			count = count + 1
		end

	end

	return count

end

--- Destroys this Scrappy Library.
function library:destroy()
	Runtime:removeEventListener( "mouse", self )
	Runtime:removeEventListener( "key", self )
	Runtime:removeEventListener( "axis", self )
end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Input library
if not Scrappy.Input then

	-- Then store the library out
	Scrappy.Input = library

end

-- Return the new library
return library
